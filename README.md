# fieldmeta: 基于springboot的字段元数据管理
version:beta-1.0 <br>
[演示地址](http://http://47.100.120.24:8080/)：http://47.100.120.24:8080/ <br>
[模板工程示例](https://gitee.com/klguang/template-ssje)：https://gitee.com/klguang/template-ssje <br>

[元数据](https://baike.baidu.com/item/%E5%85%83%E6%95%B0%E6%8D%AE/1946090?fr=aladdin)（Metadata），
又称中介数据、中继数据，为描述数据的数据（data about data），
fieldmeta 就是描述数据库字段、实体字段、页面字段的属性和关系的数据，以及配置字段映射规则、约束校验规则、显示查询规则等。

<br>
程序开发离不开对数据的管理，充分利用fieldmeta可大大提高开发的效率，fieldmeta定位于成为程序员的第二类IDE。


#### fieldmeta可以做什么？

- 可以作为一个通用代码生成的框架，适应与任何框架和语言，比如经典技术选型：springboot+ mybaits+ ant-design，抑或是ruby、C++等语言，然后制订相关程序代码的模板和生成规则，就可方便的生成CRUD代码。
- 可以作为一个快速开发平台的引擎，通过对字段的配置，自动实现CRUD功能。
- 可以作为一个模板超市，包括admin UI模板和服务器后端模板，定位于amdin系统的快速开发。

#### 部署说明
- 下载本项目，进入Docker目录，
```
docker-compose up -d
```
配置信息查看Docker目录下.env文件。
更多docker学习资料参见，https://yeasy.gitbook.io/docker_practice/compose/install

#### 本项目技术选型
- 运行环境：jdk1.7
- 数据库：mysql 5.7
- java框架：springboot 1.5.6.RELEASE
- 持久层：spring-data-jpa 
- 前端框架：easyui 1.5.3

#### 项目结构
```
fieldmeta
├──src/main/java      
│    ├─common     公共模块
│    │ 
│    ├─fieldmeta  元数据模块
│    │ 
│    └─sys        数据字典模块 
│
├──src/main/resources 
│    ├─app-jpa.xml             spring-data-jpa配置
│    │ 
│    ├─application.properties  springboot配置
│    │ 
│    └─jdbc.properties         数据库配置
│
├──src/main/webapp jsp页面
```

#### 更新日志
2018-06-12 发布Alpha 0.0.1

#### 使用说明
1. 编写配置和模板文件
![templatefile.png](project-preview%2Ftemplatefile.png)
2. 实体元数据管理
![tablemeta.png](project-preview%2Ftablemeta.png)
3. 页面字段配置
![pagefield.png](project-preview%2Fpagefield.png)
4. 可选字段，平时可根据项目规范或习惯提前配置好
![optionfield.png](project-preview%2Foptionfield.png)
----------------------------------------------------
![option_field2.png](project-preview%2Foption_field2.png)

#### 社群
qq群：830635442
<br>
![qq_fieldmeta.jpg](project-preview%2Fqq_fieldmeta.jpg)
<br>
进群须知：star、watch、fork项目，可进群

