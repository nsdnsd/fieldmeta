<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
    	<title>${tablemeta.simpleName}</title>
   		<c:import url="/admin/pages/common/headsource.jsp"/>		
  	</head>
<body>
	<table id="datagrid-table" class="easyui-datagrid" title="${tablemeta.simpleName}列表"
		data-options="
			rownumbers		: true,
			singleSelect	: true,
			fitColumns		: true, 
			url				: adminActionPath + '/${entityNameOfAllLowcase}/findpage',
			toolbar			: '#toolbar',
			fit				: true,
			pagination		: true,
			pageSize		: 15,
			pageList        : [10,15,20,25,30],
			showFooter		: true,
			idField			: '${pkColumn.attrName}',
			onDblClickRow   : function(){dataTable.edit();}">
		<thead>
			<tr>

<#list allPageFields as pageField>
<#if pageField.canList == "yes">
	<#if (pageField.codeClass?has_content)>
				<th data-options="field:'${pageField.entityField.attrName}',width:100,align:'left',formatter:codeCol,codeClass:'${pageField.codeClass}'">${pageField.fieldTitle}</th>
	<#else>
				<th data-options="field:'${pageField.entityField.attrName}',width:100,align:'left',formatter:${pageField.fieldFormatter}">${pageField.fieldTitle}</th>
	</#if>
</#if>	
</#list>
			</tr>
		</thead>
	</table>
	<div id="toolbar">
	
		<div class="easyui-panel"
		    data-options="collapsible:true,minimizable:true">
			<form id="search-form" class="search-form" enctype="multipart/form-data">    
			    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-reload" plain="true"
					style="color: red"  onclick="resetForm('search-form')">条件重置</a>	
		
<#list allPageFields as pageField>
	<#if pageField.canQuery == "yes">
				<label>${pageField.fieldTitle}</label>
		<#if (pageField.codeClass?has_content)>
				<input name="${pageField.entityField.attrName}" class="easyui-combobox"
				data-options="valueField:'code',textField:'name',editable:false,panelHeight:'auto',enableNull:true,defaultFirst:true,codeClass:'${pageField.codeClass}'">
		<#elseif (pageField.formType=="input_date") || (pageField.formType=="input_datetime")>
				<input name="${pageField.entityField.attrName}" class="easyui-datebox">			
		<#else>
				<input name="${pageField.entityField.attrName}" style="width:125px;" type="text">
		</#if>
				<span class="inline-clear"></span>
	</#if>
</#list>
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="dataTable.search()">查询</a>
		     </form>
		</div>
	
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="dataTable.add()">添加</a>  
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="dataTable.edit()">修改</a>  
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="dataTable.remove()">删除</a>

	</div>
	
	<div id="data-form-dlg" class="easyui-dialog" style="width: 600px; height: 400px; padding: 10px 20px" closed="true"
		buttons="#dlg-buttons" modal="true">
		<form id="data-form" class="data-form" method="post">
<#list allPageFields as pageField>
	<#if pageField.formType == "input_hidden">
			<input name="${pageField.entityField.attrName}" style="display: none" />
	</#if>
</#list>	
			<table style="margin-left:-20px;">
<#list allPageFields as pageField>
	<#if (pageField.canEdit == "yes") && (pageField.formType != "input_hidden")>
				<tr class="tr_padding">
					<td><label>${pageField.fieldTitle}</label></td>
					<td <#if (pageField.formType=="input_textarea")> colspan="3"</#if> >
		<#if (pageField.codeClass?has_content)>
						<input name="${pageField.entityField.attrName}" class="easyui-combobox" <#if (pageField.entityField.notNullRestrict=="yes")>required=true</#if>
						data-options="valueField:'code',textField:'name',editable:false,panelHeight:'auto',
							defaultFirst:true,codeClass:'${pageField.codeClass}'">
		<#elseif (pageField.formType=="input_textarea")>
						<textarea rows="3" name="${pageField.entityField.attrName}" class="textarea easyui-validatebox" <#if (pageField.entityField.notNullRestrict=="yes")>required=true</#if>
							style="width: 375px"></textarea>
		<#elseif (pageField.formType=="input_date") || (pageField.formType=="input_datetime")>
						<input name="${pageField.entityField.attrName}" class="easyui-datebox" <#if (pageField.entityField.notNullRestrict=="yes")>required=true</#if>>			
		<#else>
						<input name="${pageField.entityField.attrName}" class="easyui-validatebox" <#if (pageField.entityField.notNullRestrict=="yes")>required=true</#if> <#if (pageField.fieldValidation?has_content)>validType="${pageField.fieldValidation}"</#if>>
		</#if>
					</td>
				</tr>
	</#if>
</#list>
			</table> 
		</form>
	</div>
	<div id="dlg-buttons">
		<a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-ok" onclick="dataTable.save()">保存</a> 
		<a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#data-form-dlg').dialog('close')">取消</a>
	</div>
	<script type="text/javascript">	
		
		var dataTable = new DataTable({
			$datagrid_table :$("#datagrid-table"),
			$data_form_dialog : $("#data-form-dlg"),
			$data_form : $("#data-form"),
			data_form_name : "${tablemeta.simpleName}",
			
			addOpt : {
				url : adminActionPath+"/${entityNameOfAllLowcase}/add"
			},
			editOpt : {
				url : adminActionPath+"/${entityNameOfAllLowcase}/edit"
			},
			removeOpt : {
				url : adminActionPath+"/${entityNameOfAllLowcase}/delete"
			},
			saveOpt : {},
			searchOpt : {
				$searchForm : $("#search-form"),
			}
		});
		
	</script>
</body>
</html>
			