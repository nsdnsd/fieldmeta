-- ----------------------------
-- Table structure for ${tablemeta.tableName}
-- ----------------------------
DROP TABLE IF EXISTS `${tablemeta.tableName}`;
CREATE TABLE `${tablemeta.tableName}`(

<#list allEntityFields as entityField>
	`${entityField.columnName}`	${entityField.columnType}(${entityField.length!'0'}<#if entityField.decimalPlaces??>,${entityField.decimalPlaces}</#if>)<#if (entityField.notNullRestrict=="yes")>	NOT NULL <#else> NULL DEFAULT NULL</#if><#if entityField.comments??>	COMMENT '${entityField.comments}'</#if><#if entityField.pkRestrict=="yes">	AUTO_INCREMENT</#if>,
</#list>

PRIMARY KEY (`${pkColumn.columnName}`) USING BTREE
)ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;
<#list allEntityFields as entityField>
	<#if (entityField.uniqueRestrict == "yes")> 
CREATE UNIQUE INDEX `uk_${entityField.columnName}` ON `${tablemeta.tableName}`(`${entityField.columnName}`) USING BTREE;
	</#if>
</#list>


